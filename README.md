# Proyecto 1
Este proyecto se basa en un modelo epidemiológico llamado "SIR" el cual representar de manera superficial una pandemia. De este modo, se imprimiran los datos de personas infectadas, sanas y recuperadas diariamente en forma de pasos (steps). Para lograrlo se emplearon las siguientes clases:

Comunidad, encargada de almacenar todos los datos respecto al espacio muestral de nuestra poblacion a analizar, en ella encontramos los números de habitantes, contagios iniciales, habitantes sanos. 
En esta sección se realizan los procesos necesarios para definir quien se ha infectado y/o recuperado según el paso (o dia) en que ocurra. 

Persona, util para definir al individuo con los datos ID (numero unico para cada persona lo cual los diferencia entre ellos), el estado en que se encuentra (enfermo, recuperado, sano), el contador (lo cual va dentro del constructor).

Enfermedad, encargada de definir la probabilidad de infeccion que existe en entre un contacto estrecho.

Simulador, la cual define los pasos y sus limites en la funcion "run()", donde procesara los valores que se imprimiran en los pasos.

Main, clase fundamental que correra el codigo.

## Construido con 🛠️
Este proyecto fue desarrollado utilizando el lenguaje Java en su versión 15.0.2 del paquete openjdk 15.0.2. Utilizando el IDE Eclipse en la version 2021‑03.

## Pre-requisitos
Para poder correr el programa adecuadamente se necesita:
-Sistema operativo Linux (por recomendacion Ubunu¿tu 20.04)
-Tener instalado el lenguaje de Java, lo cual puede hacerse desde la terminal mediante "sudo apt update > java -version > sudo apt install default-jre", además, requerirá de un "kit de desarrollo Java (JDK)" para compilar y ejecutar los programas, esto se hace agregando los siguientes comando a su terminal "sudo apt install default-jdk > javac -version > javac 11.0.7".
-Requerira de un IDE (como lo puede ser Eclipse), editor de texto o la propia terminal para compilar y ejecutar este codigo.
-Necesitará clonar el repositorio "https://gitlab.com/B3nj4/proyecto01-java" en su terminal para poder acceder a los archivos .java "Comunidad.java", "Persona.java", "Simulacion.java", "Main.java" ,"Enfermedad.java".
-Ya importados, se requerirá compilar y ejecutar el archivo "Main.java". Si esta utilizando la terminal deberá ingresar el comando "Javac Main.java". En cambio, si hace uso de una IDE/editor de textos deberá ver el manual de este con la finalidad de encontrar las opciones de ejecución.

## Comentarios📢
Agradecimientos a Arturo Lobos.

## Autores
- Benjamín Fisher
- Benjamín Vera
- Michelle Valdés

*Estudiantes de Ingeniería Civil en Bioinformática*
