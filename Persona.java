public class Persona{

    private Comunidad comunidad;
    private int id_;
    private int cont;
    private Enfermedad dis;
    private boolean recuperado;
    private boolean enfermo;

    Persona(Comunidad com, int id_, Enfermedad dis, int cont){
    	this.cont = cont;
        this.comunidad = com;
        this.id_ = id_;
        this.dis = dis;
        this.recuperado = false;
        this.enfermo = false;
    }
    
    public Comunidad getComunidad() {
    	return this.comunidad;
    }

    public void setEnfermo(boolean enfermo){
        this.enfermo = enfermo;
    }
    

    public int getId(){
        return this.id_;
    }
    
    public boolean getEnfermo(){
        return this.enfermo;
    }
    
    public void setRecuperado(boolean a){
    	this.recuperado = a;
    }
    
    public boolean getRecuperado(){
        return this.recuperado;
    }
    
    public Enfermedad getEnfermedad(){
        return this.dis;
    }
        
    public void contador(){
    	this.cont++;
    }
    
    public int getContador() {
    	return this.cont;
    }
}
