public class Simulador {
	
	Comunidad com;
	
	Simulador(Comunidad com){
		this.com = com;
	}
	
	//Metodo simulador
	public void run(int pasos){
		for(int i = 0; i<pasos ; i++){
                    System.out.println("Dia: " + (i+1));
			com.simular();
			if(i!=0){
				com.recuperar();
			}
		}
	}
}
