public class Enfermedad{

    private double infeccion;
    
    Enfermedad(double prob_infeccion){
        this.infeccion = prob_infeccion;
    }

    public double getInfeccion(){
        return this.infeccion;
    }
}
