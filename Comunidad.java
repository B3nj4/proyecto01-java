import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Comunidad{

	private ArrayList<Persona> infectados;
	private ArrayList<Persona> estrechos;
	private ArrayList<Persona> personas;
	private Random rd = new Random();
	private Enfermedad enfermedad;
	private int infectados_actuales;
	private int prom_estrechos;
    private int recuperados;
    private int habitantes;
    private int sanos;    
    private double prob_fisico;
    
    Comunidad(int habitantes, Enfermedad e, int enfermos_iniciales, int prom_estrechos, double p_fisico){
        this.habitantes = habitantes;
        this.personas = new ArrayList<Persona>();
        this.infectados = new ArrayList<Persona>();
        this.estrechos = new ArrayList<Persona>();
        this.prom_estrechos = prom_estrechos;
        this.prob_fisico = p_fisico;
        this.enfermedad = e;

        //Se llena de habitantes a la comunidad
        for (int i=1; i<=habitantes; i++){
            this.personas.add(new Persona(this, i, this.enfermedad,0));
        }

        //Aqui se desordena la lista
        Collections.shuffle(this.personas);

        //Aqui se infectan a los n primeras personas de la lista desordenada
        for (int i=0; i<enfermos_iniciales; i++){
            this.personas.get(i).setEnfermo(true);
        }
    }

    //Aqui se infectan y se cuentan a las personas
    public void simular(){	
        obtenerInfectados();  
        obtenerContactoEstrecho();
        contar();  
        }
          
    //Aqui se cuentan a los enfermos y sanos de la poblacion
    private void contar(){
    	this.infectados_actuales = 0;
    	for(int i = 0; i < this.habitantes; i++){
    		if(personas.get(i).getEnfermo()){
    			this.infectados_actuales++;
    		}
        }
        
        this.sanos = this.habitantes - this.infectados_actuales - this.recuperados;
        System.out.println("El total de gente susceptible a la enfermedad es de: " + ANSI_GREEN + this.sanos +ANSI_RESET + "\n" + 
        		           "El total de casos activos son: "+ ANSI_RED + this.infectados_actuales + ANSI_RESET + "\n" +
        		           "El total de personas recuperadas es de: " + ANSI_BLUE + this.recuperados + ANSI_RESET + "\n");
    }

    private void obtenerInfectados(){
        // Se limpia antes de cada pasada para no repetir gente
        this.infectados.clear();
        // Se buscan todos los infectados en la poblacion y se guardan
        // en la lista "infectados"
        for (Persona p: this.personas){
            if(p.getEnfermo()){
                this.infectados.add(p);
            }
        }
    }

    private void obtenerContactoEstrecho(){

        // Por cada persona de la lista infectados, se obtiene un grupo pequeño
        // de contactos estrechos.
        for(Persona p: this.infectados){
            Collections.shuffle(this.personas);
            for(int i=0; i<this.prom_estrechos; i++){
                this.estrechos.add(this.personas.get(i));
            } 

            comprobarInfeccion(p, this.estrechos);
            this.estrechos.clear();
        }
    }

    private void comprobarInfeccion(Persona infectado, ArrayList<Persona> estrechos){
        
        for (Persona p: estrechos){
            //Si la persona no esta ya infectada ni se encuentra recuperada es susceptible
            if (!p.getRecuperado() && !p.getEnfermo()){
                //Se comprueba que exista un contacto fisico segun aleatoriedad
                if(this.rd.nextDouble() < this.prob_fisico){
                    //Se comprueba si realmente se pego la enfermedad
                    if(this.rd.nextDouble() < infectado.getEnfermedad().getInfeccion()){
                    	p.setEnfermo(true);
                    }
                }
            }
        }	
    }
    
    //Metodo que nos permite recuperar a enfermos
    public void recuperar(){
		for(int x = 0; x<this.habitantes; x++){
			if (personas.get(x).getEnfermo()) {
				personas.get(x).contador();
				if(personas.get(x).getContador() == 5){
					personas.get(x).setRecuperado(true);
					personas.get(x).setEnfermo(false);
					this.recuperados++;
					this.infectados_actuales--;
				}
			}
		}
    }
    
    //EXTRAS color de los caracteres en el print
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_BLUE = "\u001B[34m";
}
