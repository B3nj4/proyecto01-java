public class Main{

    public static void main(String[] args){

        Enfermedad e = new Enfermedad(0.3);
        Comunidad com = new Comunidad(100, e, 10, 5, 0.7);
        Simulador sim = new Simulador(com);
        sim.run(20);
    }
}
